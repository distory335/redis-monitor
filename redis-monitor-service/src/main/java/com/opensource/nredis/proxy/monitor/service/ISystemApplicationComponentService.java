package com.opensource.nredis.proxy.monitor.service;

import com.opensource.nredis.proxy.monitor.model.SystemApplicationComponent;

/**
* service interface
*
* @author liubing
* @date 2017/01/02 15:27
* @version v1.0.0
*/
public interface ISystemApplicationComponentService extends IBaseService<SystemApplicationComponent>,IPaginationService<SystemApplicationComponent>  {
}

